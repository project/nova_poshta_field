Nova Poshta Field Module
===========================

This is a module provide Nova Poshta Field for select City and Warehouse.
And provides new Http Nova Poshta Client with others remote functions.

Install
===========================
Install with composer for load all dependencies.

Example
==========================

$this->nova_poshta = \Drupal::service('nova_poshta_field.nova_poshta')
      ->getNovaPoshta();

$cities = $this->nova_poshta->getCities($filters)->toArray();
$warehouses = $this->nova_poshta->getWarehouses($filters)->toArray();

See method \Drupalway\NovaPoshta\NovaPoshtaClient\getNovaPoshta and
See description docroot/vendor/drupalway/nova-poshta-guzzle/src/Resources/novaposhta-json-v2.php
for provide others Nova Poshta Feature.

