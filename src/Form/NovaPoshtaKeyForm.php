<?php

namespace Drupal\nova_poshta_field\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure NovaPoshta settings for this site.
 *
 * @internal
 */
class NovaPoshtaKeyForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nova_poshta_key_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['nova_poshta_field.api_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $key = $this->config('nova_poshta_field.api_key');
    $form['nova_poshta_key'] = [
      '#type' => 'textfield',
      '#default_value' => $key->get('key'),
      '#title' => t('Nova Poshta api key'),
      '#description' => t('Enter your nova poshta api key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('nova_poshta_field.api_key')
      ->set('key', $form_state->getValue('nova_poshta_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
