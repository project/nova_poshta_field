<?php

namespace Drupal\nova_poshta_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Render\Element;

/**
 * Plugin implementation of the 'nova_poshta_field' widget.
 *
 * @FieldWidget(
 *   id = "nova_poshta_field_widget",
 *   label = @Translation("Nova Poshta Field widget"),
 *   field_types = {
 *     "nova_poshta_field"
 *   }
 * )
 */
class NovaPoshtaFieldDefaultWidget extends WidgetBase {

  /**
   * How match backspace in raw value.
   */
  const DEFAULT_SPACE = 320;

  /**
   * Drupal\nova_poshta_field\NovaPoshta
   */
  public $nova_poshta;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Ignore default value
    if (isset($form['#parents'][0]) && $form['#parents'][0] == 'default_value_input') {
      return [];
    }

    // Default array for warehouse.
    $warehouse_option = [];

    // Extract the values from $form_state->getValues().
    $field_name = $this->fieldDefinition->getName();
    $path = array_merge($form['#parents'], [$field_name]);
    $key_exists = NULL;
    $values = NestedArray::getValue($form_state->getValues(), $path, $key_exists);
    $city = isset($values[$delta]['city']) ? $values[$delta]['city'] : $items[$delta]->city;
    $warehouse = isset($values[$delta]['warehouse']) ? $values[$delta]['warehouse'] : $items[$delta]->warehouse;
    $city_id = isset($values[$delta]['city_id']) ? $values[$delta]['city_id'] : $items[$delta]->city_id;
    $raw_city_id = isset($values[$delta]['raw_city_id']) ? $values[$delta]['raw_city_id'] : $items[$delta]->raw_city_id;
    if ($values === NULL) {
      $storage = $form_state->getStorage();
      if (isset($storage[$field_name])) {
        $city = $storage[$field_name]['city'];
        $warehouse = $storage[$field_name]['warehouse'];
        $city_id = $storage[$field_name]['city_id'];
        $raw_city_id = $storage[$field_name]['raw_city_id'];
      }
    }
    // Extract $warehouse_option.
    if ($city_id) {
      $warehouses = $this->getWarehouse($city_id);
      if (isset($warehouses['data'])) {
        foreach ($warehouses['data'] as $item) {
          $warehouse_option[$item['description']] = $item['description'];
        }
      }
    }

    // Build id.
    $nova_poshta_field_name = 'nova-poshta-field-' . $delta;
    $element = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#prefix' => '<div id="' . $nova_poshta_field_name . '">',
      '#suffix' => '</div>',
    ];
    $element['city'] = [
      '#type' => 'textfield',
      '#title' => t('City'),
      '#autocomplete_route_name' => 'nova_poshta_field.autocomplete_city',
      '#description' => t('Select city'),
      '#default_value' => isset($city) ? $city : '',
      '#ajax' => [
        'callback' => [get_class($this), 'ajaxRefresh'],
        'disable-refocus' => TRUE,
        'event' => 'autocompleteclose',
        'wrapper' => $nova_poshta_field_name,
      ],
      '#maxlength' => 700,
    ];
    $element['warehouse'] = [
      '#type' => 'select',
      '#title' => t('Warehouse'),
      '#default_value' => isset($warehouse) ? $warehouse : '',
      '#options' => $warehouse_option,
      '#empty_option' => '- No warehouse -',
      '#description' => t('Select warehouse'),
    ];
    $element['city_id'] = [
      '#type' => 'hidden',
      '#value' => isset($city_id) ? $city_id : '',
    ];
    $element['raw_city_id'] = [
      '#type' => 'hidden',
      '#value' => isset($raw_city_id) ? $raw_city_id : '',
    ];
    $element['#element_validate'][] = [static::class, 'validateElement'];

    // Disable validate inline form for avoid error other form element.
    $type = '';
    if (isset($form['#inline_form'])) {
      $type = get_class($form['#inline_form']);
    }
    if (isset($form['#element_validate'])) {
      foreach ($form['#element_validate'] as $key => $validate_item) {
        if (in_array($type, $validate_item)) {
          unset($form['#element_validate'][$key]);
          unset($form['#commerce_element_submit'][$key]);
        }
      }
    }

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    // Get form values.
    $raw_city_id = $form_state->getValue($element['raw_city_id']['#parents']);
    $city_value = $form_state->getValue($element['city']['#parents']);
    $city_id = $form_state->getValue($element['city_id']['#parents']);
    $warehouse = $form_state->getValue($element['warehouse']['#parents']);
    if (!$city_value) {
      $form_state->setValueForElement($element['city'], NULL);
      $form_state->setValueForElement($element['city_id'], NULL);
      $form_state->setValueForElement($element['warehouse'], NULL);
      $form_state->setValueForElement($element['raw_city_id'], NULL);
      return;
    }
    // Build raw city id.
    $length = static::DEFAULT_SPACE - (strlen($city_value) + strlen($city_id));
    $new_value = NULL;
    if ($length >= 0) {
      $space_delimiter = str_repeat(' ', $length);
      $new_value = $city_value . $space_delimiter . '|' . $city_id;
    }
    if ($raw_city_id && $new_value && ($raw_city_id != $new_value)) {
      $form_state->setError($element['city'], t ( '%title: Incorrect city', ['%title' => $city_value]));
    }

    $triggering_element = $form_state->getTriggeringElement();
    $element_name = NULL;
    foreach (Element::children($element) as $name) {
      if ($element[$name]['#name'] == $triggering_element['#name']) {
        $element_name = $name;
      }
    }

    if ($triggering_element['#type'] == 'submit' || ($triggering_element['#value'] && ($element_name == 'city'))) {
      // Prepare value.
      $clear = explode('|', $city_value);
      $city = (isset($clear[0])) ? rtrim($clear[0]) : NULL;
      $ref = (isset($clear[1])) ? trim($clear[1]) : NULL;
      $new_value = (!$new_value && $length < 0) ? $city_value : $new_value;
      if ($ref || $city_id) {
        $city_id_value = ($ref) ? $ref : $city_id;
        $form_state->setValueForElement($element['city'], $city);
        $form_state->setValueForElement($element['city_id'], $city_id_value);
        $form_state->setValueForElement($element['raw_city_id'], $new_value);
      }
      else {
        $form_state->setError($element['city'], t ( '%title: City not found.', ['%title' => $triggering_element['#value']]));
      }
      if ($triggering_element['#type'] == 'submit' && empty($warehouse)) {
        $form_state->setError($element['warehouse'], t ( 'Warehouse not found.'));
      }
    }
  }

  /**
   * Ajax callback.
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#array_parents'];
    // Get delta.
    $name_array = explode('][', $triggering_element['#name']);
    array_pop($name_array);
    end($name_array);
    $delta = current($name_array);
    // If inline form.
    if (isset($form['billing_information']['profile']['rendered'])) {
      $entity_profile = $form['billing_information']['profile']['rendered']['#profile'];
      foreach ($parents as $key => $value) {
        unset($parents[$key]);
        if ($value == 'profile') {
          break;
        }
      }
      $field_array_parents = ['billing_information', 'profile', reset($parents)];
      $field_value = $form_state->getValue($field_array_parents);
      $clear_field_value[reset($parents)] = $field_value[$delta];
      $profile_form = \Drupal::service('entity.form_builder')->getForm($entity_profile, 'default', $clear_field_value);
      $city_id_parents = $triggering_element['#parents'];
      array_pop($city_id_parents);
      $city_id_parents[] = 'city_id';
      $city_id = $form_state->getValue($city_id_parents);
      $form = $profile_form;
      static::buildValueKey($form, $city_id_parents, 'city_id', $city_id);
    }
    $city = $form_state->getValue($triggering_element['#parents']);
    $new_result = static::buildValueKey($form, $parents, 'city', $city);
    array_pop($parents);
    // Set new value for correct input.
    if (isset($profile_form) and !empty($field_value[$delta])) {
      foreach ($field_value[$delta] as $item_field_name => $item_field_value) {
        $name_array[] = $item_field_name . ']';
        $parents[] = $item_field_name;
        $tmp_name = implode('][', $name_array);
        array_pop($name_array);
        $new_result = static::buildValueKey($new_result, $parents, $item_field_name, $tmp_name, '#name');
        array_pop($parents);
      }
    }

    return NestedArray::getValue($new_result, $parents);
  }

  /**
   * Help method set new value in array.
   *
   * @param array $input
   *   form element or element where need change value.
   *
   * @param array $parents
   *   parents array.
   *
   * @param string $field
   *   last element name where need change value.
   *
   * @param mixed $value
   *   set this value.
   *
   * @param string $field_name
   *
   * @return array|mixed
   */
  public static function buildValueKey(array $input, $parents, $field = NULL, $value = NULL, $field_name = '#value') {
    $input_res = $input;
    $parents_array_value = [];
    $current = [];
    $old = NULL;
    foreach ($parents as $parent) {
      if (isset($input_res[$parent])) {
        $input_res = $input_res[$parent];
        if ($field == $parent) {
          if (isset($input_res[$field_name])) {
            $input_res[$field_name] = $value;
          }
        }
      }
      $parents_array_value[$parent] = $input_res;
    }
    $new = array_reverse($parents_array_value);
    foreach ($new as $name => &$item) {
      if (key_exists ($old, $item)) {
        $new[$name][$old] = $current;
        unset($new[$old]);
      }
      $current = $item;
      $old = $name;
    }

    return $new;
  }

  /**
   * Build Client and get Warehouses.
   *
   * @param $ref
   *
   * @return mixed
   */
  public function getWarehouse($ref) {
    $this->nova_poshta = \Drupal::service('nova_poshta_field.nova_poshta')
      ->getNovaPoshta();

    $filters = [
      'filters' => [
        'city_id' => $ref,
      ],
    ];

    return $this->nova_poshta->getWarehouses($filters)->toArray();
  }

}
