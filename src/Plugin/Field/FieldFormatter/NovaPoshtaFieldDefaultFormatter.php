<?php

namespace Drupal\nova_poshta_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'nova_poshta_field' formatter.
 *
 * @FieldFormatter(
 *   id = "nova_poshta_field_formatter",
 *   label = @Translation("FieldFormatter Label"),
 *   field_types = {
 *     "nova_poshta_field"
 *   }
 * )
 */
class NovaPoshtaFieldDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta]['city'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => 'City: ' . $item->city,
      ];
      $elements[$delta]['warehouse'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => 'Warehouse: ' . $item->warehouse,
      ];
    }

    return $elements;
  }

}
