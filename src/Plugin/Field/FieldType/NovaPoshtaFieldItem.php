<?php

namespace Drupal\nova_poshta_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Plugin implementation of the 'nova_poshta_field' field type.
 *
 * @FieldType(
 *   id = "nova_poshta_field",
 *   label = @Translation("Nova Poshta Field"),
 *   category = @Translation("Address"),
 *   description = @Translation("This field stores city and warehouse"),
 *   default_widget = "nova_poshta_field_widget",
 *   default_formatter = "nova_poshta_field_formatter"
 * )
 */
class NovaPoshtaFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'city' => [
          'description' => 'The city where is warehouses',
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
        ],
        'warehouse' => [
          'description' => 'The warehouse',
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
        ],
        'city_id' => [
          'description' => 'The city_id',
          'type' => 'varchar',
          'length' => 36,
          'not null' => TRUE,
        ],
        'raw_city_id' => [
          'description' => 'The raw city_id',
          'type' => 'varchar',
          'length' => 320,
          'not null' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('city')->getValue();
    return $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['city'] = DataDefinition::create('string')
      ->setLabel(t('City'));

    $properties['warehouse'] = DataDefinition::create('string')
      ->setLabel(t('Warehouse'));

    $properties['city_id'] = DataDefinition::create('string')
      ->setLabel(t('City id'));

    $properties['raw_city_id'] = DataDefinition::create('string')
      ->setLabel(t('Raw City id'));

    return $properties;
  }

}
