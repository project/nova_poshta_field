<?php

namespace Drupal\nova_poshta_field\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Html;

/**
 * Provides NovaPoshta AutocompleteCity.
 */
class NovaPoshtaField extends ControllerBase {

  protected $nova_poshta;
  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->nova_poshta = \Drupal::service('nova_poshta_field.nova_poshta');
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocompleteCity(Request $request) {
    $results = [];
    if ($input = $request->query->get('q')) {
      $filters = [
        'filters' => [
          'search_text' => $input,
        ],
      ];
      $cities = $this->nova_poshta->getNovaPoshta()->getCities($filters)->toArray();
      foreach ($cities['data'] as $item) {
        $length = 320 - (strlen($item['description']) + strlen($item['city_id']));
        $space_delimiter = str_repeat(' ', $length);
        $label = Html::escape($item['description'] . '(' . $item['SettlementTypeDescription'] . ')');
        $results[] = [
          'value' => $item['description'] . $space_delimiter . '|' . $item['city_id'],
          'label' => $label,
        ];
      }
    }

    return new JsonResponse($results);
  }

}
