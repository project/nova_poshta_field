<?php

namespace Drupal\nova_poshta_field;

use Drupalway\NovaPoshta\NovaPoshtaClient;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class NovaPoshta build NovaPoshta client.
 *
 * @package Drupal\nova_poshta_field
 */
class NovaPoshta {

  protected static $nova_poshta;

  protected $key;

  public function __construct(ConfigFactory $key) {
    $key = $key->get('nova_poshta_field.api_key')->get('key');
    $this->setKey($key);
    static::$nova_poshta = $this->setNovaPoshta($this->key);
  }

  /**
   * Sets a new container.
   *
   * @param null $key
   * @param array $config
   * @param array $description
   *
   * @return \Drupalway\NovaPoshta\NovaPoshtaClient
   */
  private function setNovaPoshta($key = NULL, $config = [], $description = []) {
    $key = ($key) ? $key : $this->getKey();
    return NovaPoshtaClient::factory([
      'defaults' => [
        'api_key' => $key,
      ],
    ],
      $config,
      $description
      );
  }

  /**
   * Get container or create.
   *
   * @param null $key
   * @param array $config
   * @param array $description
   *
   * @return mixed
   */
  public function getNovaPoshta($key = NULL, $config = [], $description = []) {
    if (static::$nova_poshta === NULL) {
      static::$nova_poshta = $this->setNovaPoshta($key, $config, $description);
      return static::$nova_poshta;
    }
    return static::$nova_poshta;
  }

  /**
   * Unset nova poshta client.
   */
  public function unsetNovaPoshta() {
    static::$nova_poshta = NULL;
  }

  /**
   * Get key.
   *
   * @return mixed
   */
  private function getKey() {
    return $this->key;
  }

  /**
   * Set key.
   *
   * @param $key
   */
  private function setKey($key) {
    $this->key = $key;
  }

}
